﻿using todo_app.Domain;

namespace todo_app.Mvc.Models.Repository
{
    public interface ITodoRepository
    {
        public Task<Todo> AddAsync(Todo item);
        public Task<Todo> UpdateAync(Todo item);
        public Task<Todo> DeleteAsync(Todo item);
        public IQueryable<Todo> SelectAll();
        public Task<Todo> SeclectByIdAsync(Guid id);
    }
}
