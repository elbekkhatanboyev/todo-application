﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using todo_app.Domain;

namespace todo_app.Mvc.Models.Repository
{
    public class TodoRepository : ITodoRepository
    {
        private readonly AppDbContext dbContext;
        private readonly DbSet<Todo> dbSet;

        public TodoRepository(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
            this.dbSet = dbContext.Set<Todo>();
        }

        public async Task<Todo> AddAsync(Todo item)
        {
            EntityEntry<Todo> entryEntity = await this.dbSet.AddAsync(item);
            await this.dbContext.SaveChangesAsync();

            return entryEntity.Entity;
        }

        public async Task<Todo> DeleteAsync(Todo item)
        {
            EntityEntry<Todo> entryEntity = this.dbSet.Remove(item);
            await this.dbContext.SaveChangesAsync();

            return entryEntity.Entity;
        }

        public async Task<Todo> SeclectByIdAsync(Guid id)
        {
            return await this.dbSet.FindAsync(id);
        }

        public IQueryable<Todo> SelectAll()
        {
            return this.dbSet;
        }

        public async Task<Todo> UpdateAync(Todo item)
        {
            EntityEntry<Todo> entryEntity = this.dbSet.Update(item);
            await this.dbContext.SaveChangesAsync();

            return entryEntity.Entity;
        }
    }
}
