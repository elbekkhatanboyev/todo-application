﻿using Microsoft.EntityFrameworkCore;
using todo_app.Domain;

namespace todo_app.Mvc.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) 
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Todo>().HasData(
                new Todo
                {
                    Id = Guid.NewGuid(),
                    Title = "Finish project",
                    Description = "Complete the project by the deadline",
                    DueTime = DateTime.Now.AddDays(7),
                    CreatedAt = DateTime.Now,
                    Status = TodoStatus.InProgress
                },
                new Todo
                {
                    Id = Guid.NewGuid(),
                    Title = "Buy groceries",
                    Description = "Purchase items for the week",
                    DueTime = DateTime.Now.AddDays(4),
                    CreatedAt = DateTime.Now,
                    Status = TodoStatus.NotStarted
                });
        }

        public DbSet<Todo> Todos { get; set; }
    }
}
