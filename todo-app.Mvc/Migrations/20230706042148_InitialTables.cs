﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace todo_app.Mvc.Migrations
{
    /// <inheritdoc />
    public partial class InitialTables : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Todos",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DueTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Todos", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Todos",
                columns: new[] { "Id", "CreatedAt", "Description", "DueTime", "Status", "Title" },
                values: new object[,]
                {
                    { new Guid("9aad5cb4-7063-436a-8013-8c036719455c"), new DateTime(2023, 7, 6, 9, 21, 48, 310, DateTimeKind.Local).AddTicks(5586), "Complete the project by the deadline", new DateTime(2023, 7, 13, 9, 21, 48, 310, DateTimeKind.Local).AddTicks(5561), 1, "Finish project" },
                    { new Guid("f1a7a6ad-a01b-4613-b328-f38386122a2e"), new DateTime(2023, 7, 6, 9, 21, 48, 310, DateTimeKind.Local).AddTicks(5594), "Purchase items for the week", new DateTime(2023, 7, 10, 9, 21, 48, 310, DateTimeKind.Local).AddTicks(5592), 0, "Buy groceries" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Todos");
        }
    }
}
