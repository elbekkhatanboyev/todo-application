﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace todo_app.Mvc.Migrations
{
    /// <inheritdoc />
    public partial class UpdateColumns : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Todos",
                keyColumn: "Id",
                keyValue: new Guid("9aad5cb4-7063-436a-8013-8c036719455c"));

            migrationBuilder.DeleteData(
                table: "Todos",
                keyColumn: "Id",
                keyValue: new Guid("f1a7a6ad-a01b-4613-b328-f38386122a2e"));

            migrationBuilder.InsertData(
                table: "Todos",
                columns: new[] { "Id", "CreatedAt", "Description", "DueTime", "Status", "Title" },
                values: new object[,]
                {
                    { new Guid("38112b60-26d7-457b-94b6-5fed00c5fbfc"), new DateTime(2023, 7, 6, 18, 5, 14, 325, DateTimeKind.Local).AddTicks(8886), "Purchase items for the week", new DateTime(2023, 7, 10, 18, 5, 14, 325, DateTimeKind.Local).AddTicks(8885), 0, "Buy groceries" },
                    { new Guid("746fb8c8-5d72-4b2b-85ce-e37cb45a37ad"), new DateTime(2023, 7, 6, 18, 5, 14, 325, DateTimeKind.Local).AddTicks(8881), "Complete the project by the deadline", new DateTime(2023, 7, 13, 18, 5, 14, 325, DateTimeKind.Local).AddTicks(8846), 1, "Finish project" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Todos",
                keyColumn: "Id",
                keyValue: new Guid("38112b60-26d7-457b-94b6-5fed00c5fbfc"));

            migrationBuilder.DeleteData(
                table: "Todos",
                keyColumn: "Id",
                keyValue: new Guid("746fb8c8-5d72-4b2b-85ce-e37cb45a37ad"));

            migrationBuilder.InsertData(
                table: "Todos",
                columns: new[] { "Id", "CreatedAt", "Description", "DueTime", "Status", "Title" },
                values: new object[,]
                {
                    { new Guid("9aad5cb4-7063-436a-8013-8c036719455c"), new DateTime(2023, 7, 6, 9, 21, 48, 310, DateTimeKind.Local).AddTicks(5586), "Complete the project by the deadline", new DateTime(2023, 7, 13, 9, 21, 48, 310, DateTimeKind.Local).AddTicks(5561), 1, "Finish project" },
                    { new Guid("f1a7a6ad-a01b-4613-b328-f38386122a2e"), new DateTime(2023, 7, 6, 9, 21, 48, 310, DateTimeKind.Local).AddTicks(5594), "Purchase items for the week", new DateTime(2023, 7, 10, 9, 21, 48, 310, DateTimeKind.Local).AddTicks(5592), 0, "Buy groceries" }
                });
        }
    }
}
