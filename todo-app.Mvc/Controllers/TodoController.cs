﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using todo_app.Domain;
using todo_app.Domain.DTOs;
using todo_app.Domain.ViewModels;
using todo_app.Mvc.Models.Repository;

namespace todo_app.Mvc.Controllers
{
    [Route("[controller]")]
    public class TodoController : Controller
    {
        private readonly ITodoRepository todoRepository;

        public TodoController(ITodoRepository todoRepository)
        {
            this.todoRepository = todoRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var todos = this.todoRepository.SelectAll();
            ViewBag.today = DateTime.Today;
            ViewBag.tomorrow = DateTime.Today.AddDays(1);

            return View(todos.ToList());
        }

        [HttpGet("filter")]
        public async Task<IActionResult> FilterTodos([FromQuery] string dueDate)
        {
            var todos = this.todoRepository.SelectAll();

            if (dueDate == "today")
            {
                var todayStart = DateTime.Today;
                var todayEnd = todayStart.AddDays(1);

                var todosDueToday = this.todoRepository.SelectAll()
                    .Where(t => t.DueTime >= todayStart && t.DueTime < todayEnd)
                    .ToList();

                return View(todosDueToday);
            }

            var emptyList = Enumerable.Empty<Todo>().ToList();

            return View(emptyList);
        }

        [HttpGet("[action]")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("[action]")]
        public async Task<IActionResult> Create([FromForm] TodoCreateDto createDto)
        {
            try
            {
                if (createDto.DueTime < DateTime.Now)
                {
                    ModelState.AddModelError("DueTime", "Please select an upcoming date");
                }

                if (ModelState.IsValid)
                {
                    var todo = new Todo
                    {
                        Id = Guid.NewGuid(),
                        Title = createDto.Title,
                        Description = createDto.Description,
                        DueTime = createDto.DueTime,
                        CreatedAt = DateTime.UtcNow,
                        Status = TodoStatus.NotStarted
                    };

                    Todo entity = await this.todoRepository.AddAsync(todo);

                    return RedirectToAction("Index");
                }

                throw new ArgumentException("Invalid parameters");
            }
            catch (ArgumentException ex)
            {
                return View("Create", createDto);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Edit(Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                {
                    throw new ArgumentException("Invalid id");
                }

                Todo todo = await this.todoRepository.SeclectByIdAsync(id);

                if (todo is null)
                {
                    return NotFound();
                }

                return View(todo);
            }
            catch (ArgumentException ex)
            {
                return View("Error");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("[action]/{id}")]
        public async Task<IActionResult> Edit([FromForm] Todo todo)
        {
            try
            {
                if (todo.DueTime < DateTime.Now)
                {
                    ModelState.AddModelError("DueTime", "Please select an upcoming date");
                }

                if (ModelState.IsValid)
                {
                    Todo UpdatedTodo = await this.todoRepository.UpdateAync(todo);

                    return RedirectToAction("Index");
                }

                throw new ArgumentException("Invalid parameters");
            }
            catch (ArgumentException ex) 
            {
                return View("Edit", todo);
            }
            catch (Exception ex)
            {

                return View("Error");
            }
        }

        [HttpPost("[action]/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Todo retrivedTodo = await this.todoRepository.SeclectByIdAsync(id);

                    if (retrivedTodo is null)
                    {
                        return NotFound();
                    }

                    Todo todo = await this.todoRepository.DeleteAsync(retrivedTodo);

                    return RedirectToAction("Index");
                }

                throw new ArgumentException("Invalid parameters");
            }
            catch(ArgumentException ex)
            {
                return View("Error");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("[action]")]
        public async Task<IActionResult> ChangeStatus([FromQuery] Guid id, [FromQuery] TodoStatus status)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    Todo todo = await this.todoRepository.SeclectByIdAsync(id);
                    todo.Status = status;

                    var updatedTodo = await this.todoRepository.UpdateAync(todo);

                    return RedirectToAction("Index");
                }

                throw new ArgumentException("Invalid id or status");
            }
            catch(ArgumentException ex)
            {
                return View("Error");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }
    }
}
