﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace todo_app.Domain.ViewModels
{
    public class ChangeStatusRequestViewModel
    {
        public Guid Id { get; set; }
        public TodoStatus Status { get; set; }
    }
}
