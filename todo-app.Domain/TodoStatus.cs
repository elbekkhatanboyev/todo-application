﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace todo_app.Domain
{
    public enum TodoStatus
    {
        NotStarted,
        InProgress,
        Completed
    }
}
