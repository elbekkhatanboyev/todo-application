﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace todo_app.Domain
{
    public class Todo
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime DueTime { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        [Required]
        public TodoStatus Status { get; set; }
    }
}
