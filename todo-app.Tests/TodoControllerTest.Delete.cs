﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using todo_app.Domain;
using todo_app.Mvc.Controllers;
using todo_app.Mvc.Models.Repository;
using Xunit;

namespace todo_app.Tests
{
    public partial class TodoControllerTest
    {
        [Fact]
        public async Task DeleteWithValidIdReturnsRedirectToActionResult()
        {
            // Arrange
            Guid validId = Guid.NewGuid();
            Todo validTodo = new Todo { Id = validId, Title = "Sample Todo" };

            this.todoRepositoryMock.Setup(repo => repo.SeclectByIdAsync(validId)).ReturnsAsync(validTodo);
            this.todoRepositoryMock.Setup(repo => repo.DeleteAsync(validTodo)).ReturnsAsync(validTodo);

            var controller = new TodoController(this.todoRepositoryMock.Object);

            // Act
            var result = await controller.Delete(validId);

            // Assert
            var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Index", redirectToActionResult.ActionName);
        }

        [Fact]
        public async Task DeleteWithInvalidIdReturnsNotFoundResult()
        {
            // Arrange
            Guid invalidId = Guid.Empty;

            // Act
            var result = await this.todoController.Delete(invalidId);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }
    }
}
