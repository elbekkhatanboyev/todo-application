﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using todo_app.Domain;
using todo_app.Domain.DTOs;
using todo_app.Mvc.Controllers;
using todo_app.Mvc.Models.Repository;
using Xunit;

namespace todo_app.Tests
{
    public partial class TodoControllerTest
    {
        [Fact]
        public async Task CreateInvalidDueTimeReturnsViewWithError()
        {
            // Arrange
            var createDto = new TodoCreateDto
            {
                Title = "Test Todo",
                Description = "Test Description",
                DueTime = DateTime.Now.AddDays(-1)
            };

            // Act
            var result = await this.todoController.Create(createDto);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal("Create", viewResult.ViewName);
            Assert.Equal(createDto, viewResult.Model);

            var modelState = this.todoController.ModelState;
            Assert.True(modelState.ContainsKey("DueTime"));
            var error = modelState["DueTime"].Errors[0];
            Assert.Equal("Please select an upcoming date", error.ErrorMessage);
        }

        [Fact]
        public async Task CreateValidDueTimeRedirectsToIndex()
        {
            // Arrange
            var createDto = new TodoCreateDto
            {
                Title = "Test Todo",
                Description = "Test Description",
                DueTime = DateTime.Now.AddDays(1)
            };

            this.todoRepositoryMock.Setup(repo => repo.AddAsync(It.IsAny<Todo>()))
                .ReturnsAsync(new Todo { Id = Guid.NewGuid() });

            // Act
            var result = await this.todoController.Create(createDto);

            // Assert
            var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Index", redirectToActionResult.ActionName);
        }
    }
}
