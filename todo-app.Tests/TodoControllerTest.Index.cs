﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using todo_app.Domain;
using todo_app.Mvc.Controllers;
using todo_app.Mvc.Models.Repository;
using Xunit;

namespace todo_app.Tests
{
    public partial class TodoControllerTest
    {
        [Fact]
        public async Task IndexReturnsViewResultWithTodos()
        {
            // Arrange
            var todos = new List<Todo>
            {
                new Todo { Id = Guid.NewGuid(), Title = "Todo 1" },
                new Todo { Id = Guid.NewGuid(), Title = "Todo 2" },
                new Todo { Id = Guid.NewGuid(), Title = "Todo 3" }
            };

            this.todoRepositoryMock.Setup(repo => 
                repo.SelectAll()).Returns(todos.AsQueryable());

            // Act
            var result = await this.todoController.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<List<Todo>>(viewResult.ViewData.Model);
            Assert.Equal(todos.Count, model.Count);
        }
    }
}
