﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using todo_app.Domain;
using todo_app.Mvc.Controllers;
using todo_app.Mvc.Models.Repository;
using Xunit;

namespace todo_app.Tests
{
    public partial class TodoControllerTest
    {
        [Fact]
        public async Task FilterTodosReturnsViewResultWithFilteredTodosWhenDueDateIsToday()
        {
            // Arrange
            var todos = new List<Todo>
            {
                new Todo { Id = Guid.NewGuid(), Title = "Todo 1", DueTime = DateTime.Today },
                new Todo { Id = Guid.NewGuid(), Title = "Todo 2", DueTime = DateTime.Today.AddDays(1) },
                new Todo { Id = Guid.NewGuid(), Title = "Todo 3", DueTime = DateTime.Today }
            };

            this.todoRepositoryMock.Setup(repo => 
                repo.SelectAll()).Returns(todos.AsQueryable());

            // Act
            var result = await this.todoController.FilterTodos("today");

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<List<Todo>>(viewResult.ViewData.Model);
            Assert.Equal(2, model.Count);
        }

        [Fact]
        public async Task FilterTodosReturnsViewResultWithEmptyListWhenDueDateIsNotToday()
        {
            // Arrange
            var todos = new List<Todo>
            {
                new Todo { Id = Guid.NewGuid(), Title = "Todo 1", DueTime = DateTime.Today },
                new Todo { Id = Guid.NewGuid(), Title = "Todo 2", DueTime = DateTime.Today.AddDays(1) },
                new Todo { Id = Guid.NewGuid(), Title = "Todo 3", DueTime = DateTime.Today }
            };

            this.todoRepositoryMock.Setup(repo => 
                repo.SelectAll()).Returns(todos.AsQueryable());

            // Act
            var result = await this.todoController.FilterTodos("notToday");

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<List<Todo>>(viewResult.ViewData.Model);
            Assert.Empty(model);
        }
    }
}
