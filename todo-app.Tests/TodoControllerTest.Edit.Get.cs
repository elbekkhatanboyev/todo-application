﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using todo_app.Domain;
using todo_app.Mvc.Controllers;
using todo_app.Mvc.Models.Repository;
using Xunit;

namespace todo_app.Tests
{
    public partial class TodoControllerTest
    {
        [Fact]
        public async Task EditWithValidIdReturnsViewResult()
        {
            // Arrange
            Guid validId = Guid.NewGuid();
            Todo validTodo = new Todo { Id = validId, Title = "Sample Todo", DueTime = DateTime.UtcNow.AddDays(1) };

            this.todoRepositoryMock.Setup(repo => repo.SeclectByIdAsync(validId)).ReturnsAsync(validTodo);

            // Act
            var result = await this.todoController.Edit(validId);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsType<Todo>(viewResult.Model);
            Assert.Equal(validTodo, model);
        }

        [Fact]
        public async Task EditWithInvalidIdReturnsNotFoundResult()
        {
            // Arrange
            Guid invalidId = Guid.Empty;

            // Act
            var result = await this.todoController.Edit(invalidId);

            // Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async Task EditWithExceptionReturnsErrorView()
        {
            // Arrange
            Guid validId = Guid.NewGuid();

            this.todoRepositoryMock.Setup(repo => 
                repo.SeclectByIdAsync(validId))
                    .ThrowsAsync(new Exception("Database error"));

            var controller = new TodoController(this.todoRepositoryMock.Object);

            // Act
            var result = await controller.Edit(validId);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal("Error", viewResult.ViewName);
        }
    }
}
