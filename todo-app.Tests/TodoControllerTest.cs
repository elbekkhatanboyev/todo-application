﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using todo_app.Mvc.Controllers;
using todo_app.Mvc.Models.Repository;

namespace todo_app.Tests
{
    public partial class TodoControllerTest
    {
        private readonly Mock<ITodoRepository> todoRepositoryMock;
        private readonly TodoController todoController;

        public TodoControllerTest()
        {
            this.todoRepositoryMock = new Mock<ITodoRepository>();
            this.todoController = new TodoController(this.todoRepositoryMock.Object);
        }
    }
}
