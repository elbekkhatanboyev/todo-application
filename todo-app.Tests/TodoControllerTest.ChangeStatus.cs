﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using todo_app.Domain;
using todo_app.Mvc.Controllers;
using todo_app.Mvc.Models.Repository;
using Xunit;

namespace todo_app.Tests
{
    public partial class TodoControllerTest
    {
        [Fact]
        public async Task ChangeStatusWithValidIdAndStatusReturnsRedirectToActionResult()
        {
            // Arrange
            Guid validId = Guid.NewGuid();
            Todo validTodo = new Todo { Id = validId, Title = "Sample Todo" };
            TodoStatus validStatus = TodoStatus.Completed;

            this.todoRepositoryMock.Setup(repo => repo.SeclectByIdAsync(validId)).ReturnsAsync(validTodo);
            this.todoRepositoryMock.Setup(repo => repo.UpdateAync(validTodo)).ReturnsAsync(validTodo);

            // Act
            var result = await this.todoController.ChangeStatus(validId, validStatus);

            // Assert
            var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Index", redirectToActionResult.ActionName);
        }

        [Fact]
        public async Task ChangeStatusWithInvalidIdReturnsErrorView()
        {
            // Arrange
            Guid invalidId = Guid.Empty;
            TodoStatus validStatus = TodoStatus.Completed;

            // Act
            var result = await this.todoController.ChangeStatus(invalidId, validStatus);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal("Error", viewResult.ViewName);
        }
    }
}
